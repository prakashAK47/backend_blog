const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const Employee = mongoose.model('Category');

router.get('/', (req, res) => {
    res.render("category/addOrEdit", {
        viewTitle: "Insert category"
    });
});

router.post('/', (req, res) => {
    if (req.body._id == '')
        insertRecord(req, res);
        else
        updateRecord(req, res);
});


function insertRecord(req, res) {
    var category = new Category();
    category.addCategory = req.body.addCategory;
    
    category.save((err, doc) => {
        if (!err)
            res.redirect('category/list');
        else {
            if (err.name == 'ValidationError') {
                handleValidationError(err, req.body);
                res.render("category/addOrEdit", {
                    viewTitle: "Insert Category",
                    employee: req.body
                });
            }
            else
                console.log('Error during record insertion : ' + err);
        }
    });
}

function updateRecord(req, res) {
    Employee.findOneAndUpdate({ _id: req.body._id }, req.body, { new: true }, (err, doc) => {
        if (!err) { res.redirect('category/list'); }
        else {
            if (err.name == 'ValidationError') {
                handleValidationError(err, req.body);
                res.render("category/addOrEdit", {
                    viewTitle: 'Update Category',
                    category: req.body
                });
            }
            else
                console.log('Error during record update : ' + err);
        }
    });
}


router.get('/list', (req, res) => {
    Category.find((err, docs) => {
        if (!err) {
            res.render("category/list", {
                list: docs
            });
        }
        else {
            console.log('Error in retrieving category list :' + err);
        }
    });
});


function handleValidationError(err, body) {
    for (field in err.errors) {
        switch (err.errors[field].path) {
            case 'addCategory':
                body['fullNameError'] = err.errors[field].message;
                break;
            default:
                break;
        }
    }
}

router.get('/:id', (req, res) => {
    Employee.findById(req.params.id, (err, doc) => {
        if (!err) {
            res.render("category/addOrEdit", {
                viewTitle: "Update Category",
                category: doc
            });
        }
    });
});

router.get('/delete/:id', (req, res) => {
    Employee.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) {
            res.redirect('/category/list');
        }
        else { console.log('Error in category delete :' + err); }
    });
});

module.exports = router;